#pragma once

#include <utils/os/fs/Path.h>
#include <utils/Xml.h>
#include <utils/datetime/DateTime.h>
#include <utils/Strings.h>
#include <games/classifications/Regions.h>
#include "ItemType.h"
#include "games/classifications/Genres.h"

//#define _METADATA_STATS_

// Forward declaration (MetadataFieldDescriptor must indlude MetadataDescriptor)
class MetadataFieldDescriptor;

class MetadataDescriptor
{
  private:
    //! Default value storage for fast default detection
    static MetadataDescriptor _Default;

    #ifdef _METADATA_STATS_
    static int LivingClasses;
    static int LivingFolders;
    static int LivingGames;
    #endif

    // Static default values
    static const std::string DefaultValueRatio;
    static const std::string DefaultValueEmpty;
    static const std::string DefaultValueRating;
    static const std::string DefaultValuePlayers;
    static const std::string DefaultValuePlaycount;
    //static const std::string DefaultValueUnknown;
    static const std::string DefaultValueFavorite;
    static const std::string DefaultValueHidden;
    static const Path        DefaultEmptyPath;

    //! Game node <game></game>
    static const std::string GameNodeIdentifier;
    //! Folder node <folder></folder>
    static const std::string FolderNodeIdentifier;

    // Please keep field ordered by type size to reduce alignment padding
    std::string  _Name;         //!< Name as simple string
    std::string  _Description;  //!< Description, multiline text
    Path         _Image;        //!< Image path
    std::string  _Developer;    //!< Developer name
    std::string  _Publisher;    //!< Publisher name
    std::string* _Genre;        //!< Genres, comma separated
    std::string* _Emulator;     //!< Specific emulator
    std::string* _Core;         //!< Specific core
    std::string* _Ratio;        //!< Specific screen ratio
    Path*        _Thumbnail;    //!< Thumbnail path
    Path*        _Video;        //!< Video path
    float        _Rating;       //!< Rating from 0.0 to 1.0
    GameGenres   _GenreId;      //!< Normalized Genre
    int          _Region;       //!< Rom/Game Region
    int          _Players;      //!< Players range: LSW:from - MSW:to (allow sorting by max players)
    int          _ReleaseDate;  //!< Release data (epoch)
    int          _Playcount;    //!< Play counter
    unsigned int _LastPlayed;   //!< Last time played (epoch)
    int          _RomCrc32;     //!< Rom Crc32
    bool         _Favorite;     //!< Favorite gale
    bool         _Hidden;       //!< Hidden game
    bool         _Adult;        //!< Adult state
    bool         _Dirty;        //!< Dirty flag (modified data flag)

    ItemType     _Type;         //!< Metadata type

    /*!
     * Build an empty object filled with default values
     * @return Object filled with default values
     */
    static MetadataDescriptor BuildDefaultValueMetadataDescriptor();

    /*!
     * Free the PString if non null
     * @param string Pointer to std::string
     */
    static void FreePString(std::string*& string)
    {
      if (string != nullptr)
      {
        delete string;
        string = nullptr;
      }
    }

    /*!
     * Free the PPath if non null
     * @param path Pointer to Path
     */
    static void FreePPath(Path*& path)
    {
      if (path != nullptr)
      {
        delete path;
        path = nullptr;
      }
    }

    /*!
     * Assign a value to the given PString.
     * the PString is created/destroyed if required
     * @param string PString to assign value to
     * @param value Value to assign
     */
    static void AssignPString(std::string*& string, const std::string& value)
    {
      if (value.empty()) FreePString(string);
      else
      {
        if (string == nullptr) string = new std::string();
        *string = value;
      }
    }

    /*!
     * Assign a value to the given PPath.
     * the PPath is created/destroyed if required
     * @param string PPath to assign value to
     * @param value Value to assign
     */
    static void AssignPPath(Path*& path, const Path& value)
    {
      if (value.IsEmpty()) FreePPath(path);
      else
      {
        if (path == nullptr) path = new Path();
        *path = value;
      }
    }

    /*!
     * Read PString content. Return static empty string if the PString is null
     * @param string PString to read
     * @return read value
     */
    static const std::string& ReadPString(const std::string* string, const std::string& defaultvalue)
    {
      if (string == nullptr) return defaultvalue;
      return *string;
    }

    /*!
     * Read PPath content. Return static empty path if the PPath is null
     * @param path PPath to read
     * @return read value
     */
    static const Path& ReadPPath(const Path* path, const Path& defaultvalue)
    {
      if (path == nullptr) return defaultvalue;
      return *path;
    }

    /*!
     * Return the first static internal field descriptor reference
     * @param Type ObjectType from which to retrieve
     * @param count Number of actual field descriptor available
     * @return first static internal field descriptor reference
     */
    static const MetadataFieldDescriptor* GetMetadataFieldDescriptors(ItemType type, int& count);

    /*!
     * Convert integer range to string: low-high
     * @param range Integer range: Highest into MSB, Lowest into LSB
     * @return Converted string
     */
    static std::string IntToRange(int range);
    /*!
     * Convert a range X-Y to an int: Highest into MSB, Lowest into LSB (allow sorting by highest value)
     * @param range Range string
     * @param to destination int
     * @return True if the operation is successful. False otherwise.
     */
    static bool RangeToInt(const std::string& range, int& to);
    /*!
     * Convert int32 to Hexadecimal string
     * @param from Int32 value to convert to string
     * @param to Hexadecimal result string
     * @return True if the operation is successful. False otherwise.
     */
    static bool IntToHex(int from, std::string& to);
    /*!
     * Convert Hexa string into int32
     * @param from Hexadecimal string
     * @param to Target int32
     * @return True if the operation is successful. False otherwise.
     */
    static bool HexToInt(const std::string& from, int& to);
    /*!
     * Fast string to int conversion
     * @param from source string
     * @param to destination int
     * @param offset offset in source string
     * @param stop Stop char
     * @return True if the operation is successful. False otherwise.
     */
    static bool StringToInt(const std::string& from, int& to, int offset, char stop);
    /*!
     * Fast string to int conversion
     * @param from source string
     * @param to destination int
     * @return True if the operation is successful. False otherwise.
     */
    static bool StringToInt(const std::string& from, int& to);
    /*!
     * Fast string to float conversion
     * @param from source string
     * @param to destination float
     * @return True if the operation is successful. False otherwise.
     */
    static bool StringToFloat(const std::string& from, float& to);

    /*!
     * Free all allocated objects and return thoses objects to uninitialized state
     */
    void FreeAll();

  public:
    /*
     * Destructor
     */
    ~MetadataDescriptor();

    /*!
     * Default constructor
     */
    explicit MetadataDescriptor(const std::string& defaultName, ItemType type)
      : _Name(defaultName),
        _Description(),
        _Image(),
        _Developer(),
        _Publisher(),
        _Genre(),
        _Emulator(nullptr),
        _Core(nullptr),
        _Ratio(nullptr),
        _Thumbnail(nullptr),
        _Video(nullptr),
        _Rating(0.0f),
        _GenreId(GameGenres::None),
        _Region(0),
        _Players((1<<16)+1),
        _ReleaseDate(0),
        _Playcount(0),
        _LastPlayed(0),
        _RomCrc32(0),
        _Favorite(false),
        _Hidden(false),
        _Adult(false),
        _Dirty(false),
        _Type(type)
    {
      #ifdef _METADATA_STATS_
      LivingClasses++;
      if (_Type == ItemType::Game) LivingGames++;
      if (_Type == ItemType::Folder) LivingFolders++;
      #endif
    }

    /*!
     * Copy constructor
     * @param source Source to copy data from
     */
    #ifdef _METADATA_STATS_
    MetadataDescriptor(const MetadataDescriptor& source)
      : _Name        (source._Name       ),
        _Description (source._Description),
        _Image       (source._Image      ),
        _Developer   (source._Developer  ),
        _Publisher   (source._Publisher  ),
        _Genre       (source._Genre      ),
        _Emulator    (source._Emulator   ),
        _Core        (source._Core       ),
        _Ratio       (source._Ratio      ),
        _Thumbnail   (source._Thumbnail  ),
        _Video       (source._Video      ),
        _Region      (source._Region     ),
        _Rating      (source._Rating     ),
        _GenreId     (source._GenreId    ),
        _Players     (source._Players    ),
        _ReleaseDate (source._ReleaseDate),
        _Playcount   (source._Playcount  ),
        _LastPlayed  (source._LastPlayed ),
        _RomCrc32    (source._RomCrc32   ),
        _Favorite    (source._Favorite   ),
        _Hidden      (source._Hidden     ),
        _Adult       (source._Adult      ),
        _Dirty       (source._Dirty      ),
        _Type        (source._Type       )
    {
      LivingClasses++;
      if (_Type == ItemType::Game) LivingGames++;
      if (_Type == ItemType::Folder) LivingFolders++;
    }
    #endif

    /*!
     * Move constructor
     * @param source  Source to move data from
     */
    MetadataDescriptor(MetadataDescriptor&& source) noexcept
      : _Name        (std::move(source._Name       )),
        _Description (std::move(source._Description)),
        _Image       (std::move(source._Image      )),
        _Developer   (std::move(source._Developer  )),
        _Publisher   (std::move(source._Publisher  )),
        _Genre       (          source._Genre      ),
        _Emulator    (          source._Emulator   ),
        _Core        (          source._Core       ),
        _Ratio       (          source._Ratio      ),
        _Thumbnail   (          source._Thumbnail  ),
        _Video       (          source._Video      ),
        _Rating      (          source._Rating     ),
        _GenreId     (          source._GenreId    ),
        _Region      (          source._Region     ),
        _Players     (          source._Players    ),
        _ReleaseDate (          source._ReleaseDate),
        _Playcount   (          source._Playcount  ),
        _LastPlayed  (          source._LastPlayed ),
        _RomCrc32    (          source._RomCrc32   ),
        _Favorite    (          source._Favorite   ),
        _Hidden      (          source._Hidden     ),
        _Adult       (          source._Adult      ),
        _Dirty       (          source._Dirty      ),
        _Type        (          source._Type       )
    {
      #ifdef _METADATA_STATS_
      LivingClasses++;
      if (_Type == ItemType::Game) LivingGames++;
      if (_Type == ItemType::Folder) LivingFolders++;
      #endif
      source._Genre     = nullptr;
      source._Emulator  = nullptr;
      source._Core      = nullptr;
      source._Ratio     = nullptr;
      source._Thumbnail = nullptr;
    }

    /*!
     * Assignment operator - Required by STL objects since a copy operator is defined
     * @param source Source to copy data from
     */
    MetadataDescriptor& operator = (const MetadataDescriptor& source)
    {
      if (&source == this) return *this;

      #ifdef _METADATA_STATS_
      if (_Type == ItemType::Game) LivingGames--;
      if (_Type == ItemType::Folder) LivingFolders--;
      #endif

      FreeAll();
      _Name        = source._Name       ;
      _Description = source._Description;
      _Image       = source._Image      ;
      _Developer   = source._Developer  ;
      _Publisher   = source._Publisher  ;
      _Genre       = source._Genre      ;
      if (source._Emulator  != nullptr) _Emulator  = new std::string(*source._Emulator );
      if (source._Core      != nullptr) _Core      = new std::string(*source._Core     );
      if (source._Ratio     != nullptr) _Ratio     = new std::string(*source._Ratio    );
      if (source._Thumbnail != nullptr) _Thumbnail = new Path       (*source._Thumbnail);
      if (source._Video     != nullptr) _Video     = new Path       (*source._Video    );
      _Region      = source._Region     ;
      _Rating      = source._Rating     ;
      _GenreId     = source._GenreId    ;
      _Players     = source._Players    ;
      _ReleaseDate = source._ReleaseDate;
      _Playcount   = source._Playcount  ;
      _LastPlayed  = source._LastPlayed ;
      _RomCrc32    = source._RomCrc32   ;
      _Favorite    = source._Favorite   ;
      _Hidden      = source._Hidden     ;
      _Adult       = source._Adult      ;
      _Dirty       = source._Dirty      ;
      _Type        = source._Type       ;

      #ifdef _METADATA_STATS_
      if (_Type == ItemType::Game) LivingGames++;
      if (_Type == ItemType::Folder) LivingFolders++;
      #endif

      return *this;
    }

    /*!
     * Move operator
     * @param source Source to move data from
     */
    MetadataDescriptor& operator = (MetadataDescriptor&& source) noexcept
    {
      #ifdef _METADATA_STATS_
      if (_Type == ItemType::Game) LivingGames--;
      if (_Type == ItemType::Folder) LivingFolders--;
      #endif

      FreeAll();
      _Name        = std::move(source._Name       );
      _Emulator    = source._Emulator   ; source._Emulator = nullptr;
      _Core        = source._Core       ; source._Core     = nullptr;
      _Ratio       = source._Ratio      ; source._Ratio    = nullptr;
      _Description = std::move(source._Description);
      _Image       = std::move(source._Image      );
      _Thumbnail   = source._Thumbnail  ; source._Thumbnail = nullptr;
      _Video       = source._Video      ; source._Video     = nullptr;
      _Developer   = std::move(source._Developer  );
      _Publisher   = std::move(source._Publisher  );
      _Genre       = source._Genre      ; source._Genre     = nullptr;
      _Rating      = source._Rating     ;
      _GenreId     = source._GenreId    ;
      _Players     = source._Players    ;
      _ReleaseDate = source._ReleaseDate;
      _Playcount   = source._Playcount  ;
      _LastPlayed  = source._LastPlayed ;
      _RomCrc32    = source._RomCrc32   ;
      _Favorite    = source._Favorite   ;
      _Hidden      = source._Hidden     ;
      _Adult       = source._Adult      ;
      _Dirty       = source._Dirty      ;
      _Type        = source._Type       ;

      #ifdef _METADATA_STATS_
      if (_Type == ItemType::Game) LivingGames++;
      if (_Type == ItemType::Folder) LivingFolders++;
      #endif

      return *this;
    }

    /*!
     * Deserialize data from a given Xml node
     * @param from XML Node to deserialize from
     * @param relativeTo Root path
     * @return True of the node has been successfully deserialized
     */
    bool Deserialize(XmlNode from, const Path& relativeTo);

    /*!
     * Serialize internal data to XML node
     * @param relativeTo Root path
     * @return Serialized XML node
     */
    void Serialize(XmlNode parentTree, const Path& filePath, const Path& relativeTo) const;

    /*!
     * Merge value from the source metadata object into the current object
     * current fields ate replaced only if they have their default value.
     * @param source Metadata object from which to merge data
     */
    void Merge(const MetadataDescriptor& source);

    /*
     * Accessors
     */

    ItemType Type() const { return _Type; }

    const std::string& Name()        const { return _Name;                                        }
    const std::string& Emulator()    const { return ReadPString(_Emulator, DefaultValueEmpty);    }
    const std::string& Core()        const { return ReadPString(_Core, DefaultValueEmpty);        }
    const std::string& Ratio()       const { return ReadPString(_Ratio, DefaultValueRatio);       }
    const std::string& Description() const { return _Description;                                 }
    const Path&        Image()       const { return _Image;                                       }
    const Path&        Thumbnail()   const { return ReadPPath  (_Thumbnail, DefaultEmptyPath);    }
    const Path&        Video()       const { return ReadPPath  (_Video, DefaultEmptyPath);        }
    const std::string& Developer()   const { return _Developer;                                   }
    const std::string& Publisher()   const { return _Publisher;                                   }
    const std::string& Genre()       const { return ReadPString(_Genre, DefaultValueEmpty);       }

    float              Rating()          const { return _Rating;                           }
    int                PlayerRange()     const { return _Players;                          }
    int                PlayerMax()       const { return _Players >> 16;                    }
    int                PlayerMin()       const { return _Players & 0xFFFF;                 }
    int                ReleaseDateEpoc() const { return _ReleaseDate;                      }
    DateTime           ReleaseDate()     const { return DateTime((long long)_ReleaseDate); }
    int                PlayCount()       const { return _Playcount;                        }
    unsigned int       LastPlayedEpoc()  const { return _LastPlayed;                       }
    DateTime           LastPlayed()      const { return DateTime((long long)_LastPlayed);  }
    int                Region()          const { return _Region;                                      }
    int                RomCrc32()        const { return _RomCrc32;                         }
    bool               Favorite()        const { return _Favorite;                         }
    bool               Hidden()          const { return _Hidden;                           }
    bool               Adult()           const { return _Adult;                            }
    GameGenres         GenreId()         const { return _GenreId;                          }

    /*
     * String accessors
     */

    std::string NameAsString()        const { return _Name;                                        }
    std::string EmulatorAsString()    const { return ReadPString(_Emulator, DefaultValueEmpty);    }
    std::string CoreAsString()        const { return ReadPString(_Core, DefaultValueEmpty);        }
    std::string RatioAsString()       const { return ReadPString(_Ratio, DefaultValueRatio);       }
    std::string DescriptionAsString() const { return _Description;                                 }
    std::string ImageAsString()       const { return _Image.ToString();                            }
    std::string ThumbnailAsString()   const { return ReadPPath  (_Thumbnail, Path::Empty).ToString(); }
    std::string VideoAsString()       const { return ReadPPath  (_Video, Path::Empty).ToString();  }
    std::string DeveloperAsString()   const { return _Developer;                                   }
    std::string PublisherAsString()   const { return _Publisher;                                   }
    std::string GenreAsString()       const { return ReadPString(_Genre, DefaultValueEmpty);       }
    std::string RegionAsString()      const { return Regions::Serialize4Regions(_Region);          }

    std::string RatingAsString()      const { return Strings::ToString(_Rating, 4);                        }
    std::string PlayersAsString()     const { return IntToRange(_Players);                                 }
    std::string ReleaseDateAsString() const { return _ReleaseDate != 0 ? DateTime((long long)_ReleaseDate).ToCompactISO8601() : ""; }
    std::string PlayCountAsString()   const { return Strings::ToString(_Playcount);                           }
    std::string LastPlayedAsString()  const { return _LastPlayed != 0 ? DateTime((long long)_LastPlayed).ToCompactISO8601() : ""; }
    std::string FavoriteAsString()    const { return _Favorite ? "true" : "false";                         }
    std::string RomCrc32AsString()    const { std::string r; IntToHex(_RomCrc32, r); return r;             }
    std::string HiddenAsString()      const { return _Hidden ? "true" : "false";                           }
    std::string AdultAsString()       const { return _Adult ? "true" : "false";                            }
    std::string GenreIdAsString()     const { return Strings::ToString((int)_GenreId);                           }

    /*
     * Setters
     */

    void SetName(const std::string& name)               { _Name = name; _Dirty = true;                                  }
    void SetEmulator(const std::string& emulator)       { AssignPString(_Emulator, emulator); _Dirty = true;            }
    void SetCore(const std::string& core)               { AssignPString(_Core, core); _Dirty = true;                    }
    void SetRatio(const std::string& ratio)             { AssignPString(_Ratio, ratio); _Dirty = true;                  }
    void SetDescription(const std::string& description) { _Description = description; _Dirty = true;                    }
    void SetImagePath(const Path& image)                { _Image = image; _Dirty = true;                                }
    void SetThumbnailPath(const Path& thumbnail)        { AssignPPath(_Thumbnail, thumbnail); _Dirty = true;            }
    void SetVideoPath(const Path& video)                { AssignPPath(_Video, video); _Dirty = true;                    }
    void SetReleaseDate(const DateTime& releasedate)    { _ReleaseDate = (int)releasedate.ToEpochTime(); _Dirty = true; }
    void SetDeveloper(const std::string& developer)     { _Developer = developer; _Dirty = true;                        }
    void SetPublisher(const std::string& publisher)     { _Publisher = publisher; _Dirty = true;                        }
    void SetGenre(const std::string& genre)             { AssignPString(_Genre, genre); _Dirty = true;                  }
    void SetRating(float rating)                        { _Rating = rating; _Dirty = true;                              }
    void SetPlayers(int min, int max)
    {
      _Players = (max << 16) + min;
      _Dirty = true;
    }
    void SetRegion(int regions)                         { _Region = regions; _Dirty = true;                             }
    void SetRomCrc32(int romcrc32)                      { _RomCrc32 = romcrc32; _Dirty = true;                          }
    void SetFavorite(bool favorite)                     { _Favorite = favorite; _Dirty = true;                          }
    void SetHidden(bool hidden)                         { _Hidden = hidden; _Dirty = true;                              }
    void SetAdult(bool adult)                           { _Adult = adult; _Dirty = true;                                }
    void SetGenreId(GameGenres genre)                   { _GenreId = genre; _Dirty = true;                              }

    // Special setter to force dirty
    void SetDirty() { _Dirty = true; }

    /*
     * Volatile setters - do not set the Dirty flag for auto-saving
     */

    void SetVolatileDescription(const std::string& description) { _Description = description; }
    void SetVolatileImagePath(const Path& image) { _Image = image; }

    /*
     * String setters
     */

    void SetImagePathAsString(const std::string& image)                { _Image = image; _Dirty = true;                                }
    void SetThumbnailPathAsString(const std::string& thumbnail)        { AssignPPath(_Thumbnail, Path(thumbnail)); _Dirty = true;            }
    void SetVideoPathAsString(const std::string& video)                { AssignPPath(_Video, Path(video)); _Dirty = true;                    }
    void SetReleaseDateAsString(const std::string& releasedate)
    {
      DateTime st;
      _ReleaseDate = DateTime::FromCompactISO6801(releasedate, st) ? (int)st.ToEpochTime() : 0;
      _Dirty = true;
    }
    void SetLastPlayedAsString(const std::string& lastplayed)
    {
      DateTime st;
      _LastPlayed = DateTime::FromCompactISO6801(lastplayed, st) ? (int)st.ToEpochTime() : 0;
      _Dirty = true;
    }
    void SetRatingAsString(const std::string& rating)           { float f = 0.0f; if (StringToFloat(rating, f)) SetRating(f);              }
    void SetPlayersAsString(const std::string& players)         { if (!RangeToInt(players, _Players)) SetPlayers(1, 1);                    }
    void SetFavoriteAsString(const std::string& favorite)       { SetFavorite(favorite == "true");                                         }
    void SetHiddenAsString(const std::string& hidden)           { SetHidden(hidden == "true");                                             }
    void SetAdultAsString(const std::string& adult)             { SetAdult(adult == "true");                                             }
    void SetRomCrc32AsString(const std::string& romcrc32)       { int c; if (HexToInt(romcrc32, c)) SetRomCrc32(c);                        }
    void SetPlayCountAsString(const std::string& playcount)     { int p; if (StringToInt(playcount, p)) { _Playcount = p; _Dirty = true; } }
    void SetGenreIdAsString(const std::string& genre)           { int g; if (StringToInt(genre, g)) { _GenreId = (GameGenres)g; _Dirty = true; } }
    void SetRegionAsString(const std::string& region)           { _Region = (int)Regions::Deserialize4Regions(region); _Dirty = true; }

    /*
     * Defaults
     */

    bool IsDefaultName()            const { return _Default._Name        == _Name;        }
    bool IsDefaultEmulator()        const { return _Default.Emulator()   == Emulator();   }
    bool IsDefaultCore()            const { return _Default.Core()       == Core();       }
    bool IsDefaultRatio()           const { return _Default.Ratio()      == Ratio();      }
    bool IsDefaultDescription()     const { return _Default._Description == _Description; }
    bool IsDefaultImage()           const { return _Default._Image       == _Image;       }
    bool IsDefaultThumbnail()       const { return _Default.Thumbnail()  == Thumbnail();  }
    bool IsDefaultVideo()           const { return _Default.Video()      == Video();      }
    bool IsDefaultDeveloper()       const { return _Default._Developer   == _Developer;   }
    bool IsDefaultPublisher()       const { return _Default._Publisher   == _Publisher;   }
    bool IsDefaultGenre()           const { return _Default._Genre       == _Genre;       }
    bool IsDefaultRegion()          const { return _Default.Region()     == Region();     }
    bool IsDefaultRating()          const { return _Default._Rating      == _Rating;      }
    bool IsDefaultPlayerRange()     const { return _Default._Players     == _Players;     }
    bool IsDefaultReleaseDateEpoc() const { return _Default._ReleaseDate == _ReleaseDate; }
    bool IsDefaultPlayCount()       const { return _Default._Playcount   == _Playcount;   }
    bool IsDefaultLastPlayedEpoc()  const { return _Default._LastPlayed  == _LastPlayed;  }
    bool IsDefaultRomCrc32()        const { return _Default._RomCrc32    == _RomCrc32;    }
    bool IsDefaultFavorite()        const { return _Default._Favorite    == _Favorite;    }
    bool IsDefaultHidden()          const { return _Default._Hidden      == _Hidden;      }
    bool IsDefaultAdult()           const { return _Default._Adult       == _Adult;       }
    bool IsDefaultGenreId()         const { return _Default._GenreId     == _GenreId;     }

    /*
     * Convenient Accessors
     */

    //bool IsGame()   const { return _Type == ObjectType::Game;   }
    //bool IsFolder() const { return _Type == ObjectType::Folder; }
    bool IsDirty()  const { return _Dirty;                }

    /*
     * Convenient Methods
     */

    //static bool AreGames(const MetadataDescriptor& md1, const MetadataDescriptor& md2) { return (md1._Type == md2._Type) && md1.IsGame(); }
    //static bool AreFolders(const MetadataDescriptor& md1, const MetadataDescriptor& md2) { return (md1._Type == md2._Type) && md1.IsFolder(); }

    /*
     * Special modifiers
     */

    void IncPlaycount() { _Playcount++; _Dirty = true; }
    void SetLastplayedNow() { _LastPlayed = (unsigned int)DateTime().ToEpochTime(); _Dirty = true; }

    /*
     * Metadata FieldManagement Methods
     */

    /*!
     * Return the first static internal field descriptor reference
     * @param count Number of actual field descriptor available
     * @return first static internal field descriptor reference
     */
    const MetadataFieldDescriptor* GetMetadataFieldDescriptors(int& count) { return GetMetadataFieldDescriptors(_Type, count); }
};

